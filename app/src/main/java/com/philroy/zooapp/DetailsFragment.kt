package com.philroy.zooapp;

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.philroy.zooapp.databinding.FragmentDetailsBinding
import com.philroy.zooapp.databinding.FragmentFinishBinding


class DetailsFragment : Fragment() {
    private var _binding : FragmentDetailsBinding? = null
    private val binding : FragmentDetailsBinding get() = _binding!!
//    private val args by navArgs<HomeFragmentArgs>()


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentDetailsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initListeners()
        val args: DetailsFragmentArgs by navArgs()
        binding.textViewFact.text = args.animalDetails
        binding.textViewName.text = args.animalName

    }

    fun initListeners() = with(binding) {
        backBtn.setOnClickListener {
            val action = DetailsFragmentDirections.actionDetailsFragmentToHomeFragment()
            findNavController().navigate(action)
        }
    }

}
