package com.philroy.zooapp

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.NavDirections
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.philroy.zooapp.databinding.FragmentHomeBinding

class HomeFragment : Fragment() {
    private var _binding: FragmentHomeBinding? = null
    private val binding: FragmentHomeBinding get() = _binding!!
//    private val args by navArgs<HomeFragmentArgs>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val zooName = requireActivity().intent.getStringExtra("zooName")
        binding.zooNameString.text = "Welcome to $zooName"
        "Welcome to $zooName".toast()
        initListeners()
    }

    private fun initListeners() = with(binding) {
        btnCheetah.setOnClickListener {
            val animalName: String = "Cheetah"
            val animalDetails: String = "Cheetahs are more cute than scary."
            // not sure why I had to reorder the animal arguments in the action
            findNavController().navigate(action(animalName, animalDetails))
        }

        btnGoldenRetreiver.setOnClickListener {
            val animalName = "Doggy"
            val animalDetails = "Golden Retrievers are often kept with cheetah to reduce their stress"
            findNavController().navigate(action(animalName, animalDetails))
        }

        btnHippo.setOnClickListener {
            val animalName = "Hippo- potty- mouse"
            val animalDetails = "Vegetarians that murder everything."
            findNavController().navigate(action(animalName, animalDetails))
        }

        btnSnake.setOnClickListener{
            val animalName = "Snek"
            val animalDetails = "Sneks have 2 penises."
            findNavController().navigate(action(animalName, animalDetails))
        }

    }

    private fun action (animalName: String, animalDetails: String): NavDirections {
        return HomeFragmentDirections.actionHomeFragmentToDetailsFragment(animalDetails, animalName)
    }

    private fun String.toast() {
        Toast.makeText(activity, this, Toast.LENGTH_SHORT).show()
    }
}