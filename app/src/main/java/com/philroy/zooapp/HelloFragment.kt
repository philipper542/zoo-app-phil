package com.philroy.zooapp

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.philroy.zooapp.databinding.FragmentHelloBinding

class HelloFragment: Fragment() {
    private var _binding : FragmentHelloBinding? = null
    private val binding : FragmentHelloBinding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentHelloBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initListeners()
        Log.d(this::class.java.simpleName, "onViewCreated" )
    }

    private fun initListeners() = with(binding) {
        nextBtn.setOnClickListener {
            val action = HelloFragmentDirections.actionHelloFragmentToGettingStartedFragment(getString(R.string.zoo_name))
            findNavController().navigate(action)
        }
    }



}