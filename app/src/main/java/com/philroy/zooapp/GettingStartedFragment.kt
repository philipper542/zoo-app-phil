package com.philroy.zooapp

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.philroy.zooapp.databinding.FragmentGettingStartedBinding

class GettingStartedFragment : Fragment() {
    private var _binding: FragmentGettingStartedBinding? = null
    private val binding: FragmentGettingStartedBinding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val bundle = arguments
        if (bundle == null){
            Log.d("PassingData", "missing bundle args")
            return
        }
        val args = GettingStartedFragmentArgs.fromBundle(bundle)
        Log.d(this::class.java.simpleName, args.zooName)
    }

   override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
       _binding = FragmentGettingStartedBinding.inflate(inflater, container, false)
       return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initListeners()
        super.onViewCreated(view, savedInstanceState)
    }

    private fun initListeners() = with(binding) {
        nextBtn.setOnClickListener {
            val action = GettingStartedFragmentDirections.actionGettingStartedFragmentToFinishFragment()
            findNavController().navigate(action)
        }
        backBtn.setOnClickListener {
            val action = GettingStartedFragmentDirections.actionGettingStartedFragmentToHelloFragment()
            findNavController().navigate(action)
        }
    }
}