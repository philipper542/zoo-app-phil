package com.philroy.zooapp

import android.os.Bundle
import android.os.PersistableBundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.philroy.zooapp.databinding.ActivityOnboardingZooBinding

class ZooOnboardingActivity : AppCompatActivity() {
    private lateinit var binding: ActivityOnboardingZooBinding
    private val zooName = "the Philadelphia Zoo"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityOnboardingZooBinding.inflate(layoutInflater)
        setContentView(binding.root)
        Log.d(this::class.java.simpleName, "onCreate")

    }
}