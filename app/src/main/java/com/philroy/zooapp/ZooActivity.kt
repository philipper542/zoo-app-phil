package com.philroy.zooapp

import android.os.Bundle
import android.os.PersistableBundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import com.philroy.zooapp.databinding.ActivityZooBinding

class ZooActivity : AppCompatActivity() {
    private lateinit var binding: ActivityZooBinding
//    val navController by lazy { findNavController(R.id.homeFragment) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityZooBinding.inflate(layoutInflater)
        setContentView(binding.root)
//        navController.setGraph(R.navigation.nav_graph_zoo, intent.extras)
        Log.d("ZooActivity", "onCreate")
    }

}
