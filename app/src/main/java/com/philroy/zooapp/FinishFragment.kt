package com.philroy.zooapp;

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.philroy.zooapp.databinding.FragmentFinishBinding


class FinishFragment : Fragment() {
    private var _binding : FragmentFinishBinding? = null
    private val binding : FragmentFinishBinding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentFinishBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initListeners()
    }

    fun initListeners() = with(binding) {
        backBtn.setOnClickListener {
            val action = FinishFragmentDirections.actionFinishFragmentToGettingStartedFragment()
            findNavController().navigate(action)
        }
        finishBtn.setOnClickListener {
//
            val intent = Intent(activity , ZooActivity::class.java).apply {
                putExtra("zooName", getString(R.string.zoo_name))
            }
            startActivity(intent)
            Log.d("FinishFragment", "intent triggered for ZooActivity")
        }
    }

}
